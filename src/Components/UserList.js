import axios from "axios";
import { useState,useEffect } from "react";

const UserList = () => {

    const [listOfUser, setListOfUser] = useState([]);
    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/users')
        .then(response => {
                setListOfUser(response.data);
            })
    },[])
    return(
        <div>
            <h1>Users List</h1>
            {listOfUser.map(user => (
                <div key={user.id}>{user.name}</div>
            ))}
        </div>
    )
}

export default UserList;